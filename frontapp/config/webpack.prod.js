const {merge} = require("webpack-merge");
const baseConfig = require("./webpack.base");
const webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const fs = require('fs');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const config = merge(baseConfig, {
	mode: "production",

	output: {
		publicPath: '/',
	},

	module: {
		rules: [
			{
				test: /\.s?css$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
						options: {
							publicPath: '../',
							sourceMap: true
						}
					},
					{
						loader: "css-loader",
						options: {
							sourceMap: true,
							//url: true,
							url: (url, resourcePath) => {

								if (url.includes("assets")) {
									return true;
								}

								return false;
							},
						}
					},
					{
						loader: "postcss-loader",
						options: {
							plugins: [
								require('autoprefixer'),
								require('cssnano')({
									preset: [
										'default', {
											discardComments: {
												removeAll: true
											}
										}
									]
								})
							],
							sourceMap: true,
						}
					},
					'resolve-url-loader',
					{
						loader: "sass-loader",
						options: {
							sourceMap: true,
							additionalData: `
								@import "source/assets/scss/_mixins.scss";
								@import "source/assets/scss/_variables.scss";
							`
						}
					}
				]
			},

		]
	},

	plugins: [
		new webpack.DefinePlugin({
			'IS_LOCAL_ENV': JSON.stringify(false),
		}),
	].concat(fs.readdirSync(`${baseConfig.externals.paths.src}/view/pages/`).map(section => {
		return fs
			.readdirSync(`${baseConfig.externals.paths.src}/view/pages/${section}/`)
			.filter(name => {
				const ext = (/(?:\.([^.]+))?$/).exec(name)[1];
				return ext === 'pug'
			})
			.map(name => {
				const filename = (/([-_\w]+).\w+$/).exec(name)[1];
				return new HtmlWebpackPlugin({
					filename: `./${section}${filename === 'index' ? '' : '-' + filename}.html`,
					template: `${baseConfig.externals.paths.src}/view/pages/${section}/${filename}.pug`,
					hash: false,
					minify: false,
					inject: 'body'
				});
			})
	}).flat())
});

module.exports = new Promise((resolve, reject) => {
	resolve(config);
});