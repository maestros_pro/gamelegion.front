const {merge} = require("webpack-merge");
const baseConfig = require("./webpack.base");
const webpack = require('webpack');
const fs = require('fs');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

const config = merge(baseConfig, {
	mode: "development",
	devtool: "cheap-module-eval-source-map",

	devServer: {
		hot: true,
		open: false,
		contentBase: path.join(baseConfig.externals.paths.src, 'static')
	},

	module: {
		rules: [
			{
				test: /\.s?css$/,
				use: [
					"style-loader",
					{
						loader: "css-loader",
						options: {
							sourceMap: true,
							//url: true,
							url: (url, resourcePath) => {

								if (url.includes("assets")) {
									return true;
								}

								return false;
							},
						}
					},
					{
						loader: "postcss-loader",
						options: {
							plugins: [
								require('autoprefixer'),
							],
							sourceMap: true,
						}
					},
					'resolve-url-loader',
					{
						loader: "sass-loader",
						options: {
							sourceMap: true,
							additionalData: `
								@import "source/assets/scss/_mixins.scss";
								@import "source/assets/scss/_variables.scss";
							`
						}
					}
				]
			},

		]
	},

	plugins: [
		new webpack.DefinePlugin({
			'IS_LOCAL_ENV': JSON.stringify(true),
		}),
		new webpack.SourceMapDevToolPlugin({
			filename: "[file].map"
		})
	].concat(fs.readdirSync(`${baseConfig.externals.paths.src}/view/pages/`).map(section => {
		return fs
			.readdirSync(`${baseConfig.externals.paths.src}/view/pages/${section}/`)
			.filter(name => {
				const ext = (/(?:\.([^.]+))?$/).exec(name)[1];
				return ext === 'pug'
			})
			.map(name => {
				const filename = (/([-_\w]+).\w+$/).exec(name)[1];
				return new HtmlWebpackPlugin({
					filename: `${section}${filename === 'index' ? '' : '-' + filename}.html`,
					template: `${baseConfig.externals.paths.src}/view/pages/${section}/${filename}.pug`,
					hash: true,
					inject: 'body'
				});
			})
	}).flat())
});

module.exports = new Promise((resolve, reject) => {
	resolve(config);
});