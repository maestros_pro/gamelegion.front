
const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const paths = {
	src: path.resolve(__dirname, '../source'),
	dist: path.resolve(__dirname, '../../public'),
};

const config = {
	externals: {
		paths
	},

	entry: {
		main: ['@babel/polyfill', `${paths.src}/index.js`]
	},

	output: {
		path: paths.dist,
		filename: 'js/[name].js'
	},

	resolve: {
		extensions: ['.js', '.pug'],
		alias: {
			"~": paths.src,
		}
	},

	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: "/node_modules/",
				loader: "babel-loader",
			},
			{
				test: /\.pug$/,
				// loader: 'pug-loader'
				use:  [
					{
						loader: 'html-loader',
						options: {
							minimize: false,
						}
					},
					{
						loader: 'pug-html-loader',
						options: {
							pretty: true,
							exports: false,
							//data: require('./frontend/public/data/data.js')
						}
					}
				]
			},
			{
				test: /\.(gif|png|jpe?g|svg)$/i,
				include: [path.join(__dirname, "../source/assets/img")],	//, path.join(__dirname, "../source/assets/img")
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[name].[ext]',
							//publicPath: 'assets',
							outputPath: (url, resourcePath, context) => {
								const relativePath = resourcePath.replace(path.join(__dirname, "../source/assets/img/"), '').replace(/\\/, '/');
								return `img/${relativePath}`;
							},
						}
					},
					{
						loader: 'image-webpack-loader',
						options: {

						}
					}
				]
			},
			{
				test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
				exclude: [path.join(__dirname, "../source/assets/img")],
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[name].[ext]',
							outputPath: 'fonts/'
						}
					}
				]
			},
		]
	},

	plugins: [
		new webpack.ProvidePlugin({
			$: "jquery",
			jQuery: "jquery",
			"window.jQuery": "jquery"
		}),
		new MiniCssExtractPlugin({
			filename: `css/[name].css`
		}),
	]

};

module.exports = config;