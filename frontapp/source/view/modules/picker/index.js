import './style.scss'
import Materialize from 'materialize-css'
import moment from '../moment'

export default class Picker {
	constructor(options) {
		this.$dateTimePicker = undefined;
		this.date = undefined;
		this.value = undefined;
		this.instanceDate = undefined;
		this.instanceTime = undefined;
		this.view = '';
		this.options = options;
		this.init();
	}

	_defaultOptions() {
		return {
			datepicker: {
				container: '.offer',
				autoClose: true,

				disableDayFn: () => {
					//return new Date();
				},

				defaultDate: new Date(),
				setDefaultDate: true,
				minDate: new Date(),
				firstDay: 1,
				format: 'yyyy-dd-mm',
				i18n: {
					cancel: 'ОТМЕНИТЬ',
					clear: 'ОЧИСТИТЬ',
					done: 'СОХРАНИТЬ',
					previousMonth: '<',
					nextMonth: '>',
					months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
					monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
					weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'],
					weekdaysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
					weekdaysAbbrev: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
				},
				onOpen: (e) => {
					this.view = 'date';
					this._updateHeadInfo();
				},
				onClose: (e) => {
					this.view = '';
					this._updateHeadInfo();
					this._updateEvent();
				},
				onSelect: (e) => {
					this._updateHeadInfo();
					this.instanceTime.open();
				},
			},

			timepicker: {
				container: '.offer',
				twelveHour: false,
				fromNow: 600000,
				i18n: {
					cancel: 'ОТМЕНИТЬ',
					clear: 'ОЧИСТИТЬ',
					done: 'СОХРАНИТЬ',
				},
				onOpenEnd: (e) => {
					this.view = 'time';
					this._updateHeadInfo();
				},
				onCloseStart: (e) => {
					this.view = '';
					this._updateHeadInfo();
					this._updateEvent();
				},
				onCloseEnd: (e) => {
				},
				onSelect: () => {
					this._updateHeadInfo();
				},
			},
		}
	}

	_createDatePicker() {
		let inputDate = document.createElement('input');
		inputDate.type = 'text';
		inputDate.className = 'datetimeinput datetimeinput_date';
		inputDate.style.opacity = '0';
		inputDate.style.pointerEvents = 'none';
		inputDate.style.position = 'absolute';
		document.body.appendChild(inputDate);

		this.instanceDate = Materialize.Datepicker.init(inputDate, this._defaultOptions().datepicker);

		const $elDatePicker = $(this.instanceDate.$modalEl);

		$elDatePicker
			.addClass('datetimepicker')
			.find('.datepicker-container')
			.addClass('datetimepicker__wrap');
		$elDatePicker
			.find('.datepicker-date-display')
			.addClass('datetimepicker__head');
		$elDatePicker
			.find('.datepicker-calendar-container')
			.addClass('datetimepicker__body')
			.find('.datepicker-footer')
			.addClass('datetimepicker__footer');

		$elDatePicker
			.find('.datetimepicker__body')
			.prepend(`
				<div class="datetimepicker__body-control">
					<div class="datetimepicker__body-control-month">XXXX</div>
					<div class="datetimepicker__body-control-year">Xxxxxx</div>
					<div class="datetimepicker__body-control-arrow datetimepicker__body-control-arrow_prev"><</div>
					<div class="datetimepicker__body-control-arrow datetimepicker__body-control-arrow_next">></div>
				</div>
			`);

		$elDatePicker.on('click', '.datetimepicker__body-control-arrow_prev', () => {
			this.instanceDate.prevMonth();
			this._updateHeadInfo();
		})
		$elDatePicker.on('click', '.datetimepicker__body-control-arrow_next', () => {
			this.instanceDate.nextMonth();
			this._updateHeadInfo();
		})

		$elDatePicker
			.find('.datetimepicker__footer')
			.html(`
				<div class="datetimepicker__footer-control">
					<div class="datetimepicker__footer-control-btn datetimepicker__footer-control-btn_cancel">ОТМЕНА</div>
					<div class="datetimepicker__footer-control-btn datetimepicker__footer-control-btn_save">СОХРАНИТЬ</div>
				</div>
			`);

		$elDatePicker.on('click', '.datetimepicker__footer-control-btn_cancel', () => {
			this.instanceDate.close();
		})
		$elDatePicker.on('click', '.datetimepicker__footer-control-btn_save', () => {
			this._setValue();
		})

		if (this.$dateTimePicker) {
			this.$dateTimePicker = this.$dateTimePicker.add($elDatePicker);
		} else {
			this.$dateTimePicker = $($elDatePicker);
		}
	}

	_createTimePicker() {
		let inputTime = document.createElement('input');
		inputTime.type = 'text';
		inputTime.className = 'datetimeinput datetimeinput_time';
		inputTime.style.opacity = '0';
		inputTime.style.pointerEvents = 'none';
		inputTime.style.position = 'absolute';
		document.body.appendChild(inputTime);

		this.instanceTime = Materialize.Timepicker.init(inputTime, this._defaultOptions().timepicker);

		this.instanceTime._updateTimeFromInput();

		const $elTimePicker = $(this.instanceTime.$modalEl);

		$elTimePicker
			.addClass('datetimepicker')
			.find('.timepicker-container')
			.addClass('datetimepicker__wrap');
		$elTimePicker
			.find('.timepicker-digital-display')
			.addClass('datetimepicker__head');
		$elTimePicker
			.find('.timepicker-analog-display')
			.addClass('datetimepicker__body')
			.find('.timepicker-footer')
			.addClass('datetimepicker__footer');

		$elTimePicker
			.find('.datetimepicker__footer')
			.html(`
				<div class="datetimepicker__footer-control">
					<div class="datetimepicker__footer-control-btn datetimepicker__footer-control-btn_cancel">ОТМЕНА</div>
					<div class="datetimepicker__footer-control-btn datetimepicker__footer-control-btn_save">СОХРАНИТЬ</div>
				</div>
			`);

		$elTimePicker.on('click', '.datetimepicker__footer-control-btn_cancel', () => {
			this.instanceTime.close();
		})
		$elTimePicker.on('click', '.datetimepicker__footer-control-btn_save', () => {
			this._setValue();
		})

		if (this.$dateTimePicker) {
			this.$dateTimePicker = this.$dateTimePicker.add($elTimePicker);
		} else {
			this.$dateTimePicker = $($elTimePicker);
		}

	}

	_createEventsDateTime() {
		this.$dateTimePicker
			.on('click', '.datetimepicker__head-date', () => {
				this.setViewDate();
			})
			.on('click', '.datetimepicker__head-hour', () => {
				this.setViewHour();
			})
			.on('click', '.datetimepicker__head-minute', () => {
				this.setViewMinute();
			})
		;
	}

	_updateHeadInfo() {
		this.date = moment(this.instanceDate.date).set({hour: this.instanceTime.hours, minute: this.instanceTime.minutes});
		const view = this.instanceTime.currentView;
		const isDateView = this.view === 'date';
		const isHoursView = this.view === 'time' && view === 'hours';
		const isMinutesView = this.view === 'time' && view === 'minutes';


		this.$dateTimePicker.find('.datetimepicker__body-control-month').html(`${this._defaultOptions().datepicker.i18n.months[this.instanceDate.calendars[0].month]}`);
		this.$dateTimePicker.find('.datetimepicker__body-control-year').html(`${this.instanceDate.calendars[0].year}`);
		this.$dateTimePicker.find('.datetimepicker__head').html(`
		<div class="datetimepicker__head-inner">
			<div class="datetimepicker__head-year">
				${this.date.format('YYYY')}
			</div>
			<div class="datetimepicker__head-date ${isDateView ? 'is-active' : ''}">
				${this.date.format('MMM D')}
			</div>
			<div class="datetimepicker__head-time">
				<div class="datetimepicker__head-hour ${isHoursView ? 'is-active' : ''}">${this.date.format('HH')}</div>:<div class="datetimepicker__head-minute ${isMinutesView ? 'is-active' : ''}">${this.date.format('mm')}</div>
			</div>
		</div>
		`)
	}

	_setValue() {
		this.value = this.date;
		this.instanceTime.close();
	}

	_updateEvent() {
		if (this.value) {
			if (this.options && this.options.select) this.options.select({
				text: `${this.value.format('DD.MM.YYYY HH:mm')} МСК`,
				date: this.value.toISOString(true)
			});
		} else {
			if (this.options && this.options.cancel) this.options.cancel();
		}
	}

	setViewDate() {
		this.instanceTime.close();
		this.instanceDate.open();
		this._updateHeadInfo();
	}

	setViewHour() {
		this.instanceDate.close();
		this.instanceTime.open();
		this.instanceTime.showView('hours');
		this._updateHeadInfo();
	}

	setViewMinute() {
		this.instanceDate.close();
		this.instanceTime.open();
		this.instanceTime.showView('minutes');
		this._updateHeadInfo();
	}

	open() {
		this.setViewDate();
	}

	init() {

		this._createDatePicker();
		this._createTimePicker();
		this._createEventsDateTime();
		this._updateHeadInfo();
	}
}