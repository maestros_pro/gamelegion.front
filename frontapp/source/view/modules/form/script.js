import './style.scss'

import Inputmask from 'inputmask'


$(()=>{


	$('input[data-mask]').each(function () {
		let $t = $(this),
			inputmask = new Inputmask({
				mask: $t.attr('data-mask'),		//'+7 (999) 999-99-99',
				showMaskOnHover: false,
				onincomplete: function() {
					this.value = '';
				}
			});
		inputmask.mask($t[0]);
	});


	$('body')
		.on('click', '[data-post]', function (e) {
			e.preventDefault();
			const $t = $(this);
			const url = $t.attr('href') || $t.attr('data-post');

			$.ajax({
				type: 'post',
				url,
				success: res =>{
					window.location.reload();
				},
				error: (e)=>{

				},
				complete: ()=>{

				}
			});
		})
		.on('click', '[data-form-action]', function () {
			const $this = $(this);
			const $form = $this.closest('form');
			const action = $this.attr('data-form-action');
			if (action) $form.trigger(action);
		})
		.on('change', 'input[type="radio"], input[type="checkbox"]', function () {
			const $this = $(this);
			const $item = $this.closest('.form-input');
			if (!$item.length) return;
			const checked = $this.prop('checked');
			const $children = $item.next('.form-infield');
			const $parent = $item.closest('.form-infield');

			if ($children.length) {
				$children.find('input[type="radio"], input[type="checkbox"]').prop('checked', checked);
			}

			if ($parent.length && !checked){
				uncheckParent($parent);
			}

			function uncheckParent($el) {
				const $input = $el.prev().find('input[type="radio"], input[type="checkbox"]');
				$input.prop('checked', false);
				if ($input.closest('.form-infield').length) uncheckParent($input.closest('.form-infield'));
			}
		})
	;

	$('.form')
		.on('clear', function(){
			$(this).clear();
		})
	;

	$('[data-from-name]')
		.on('submit', function(e){
			const $form = $(this);
			const formName = $form.attr('data-from-name');
			const $submit = $form.find('[type="submit"]').length ? $form.find('[type="submit"]') : $form.find('[data-form-action="submit"]');
			let rules = {};

			switch (formName) {
				case 'login':
					rules = {
						login: [
							value => !!value || 'Укажите Логин',
							value => /^[-._a-z0-9а-я]+@(?:[a-z0-9а-я][-a-z0-9а-я]+\.)+[a-zа-я]{2,6}$/.test(value.toLowerCase()) || 'Проверьте корректнность email',
						],
						password: [
							value => !!value || 'Укажите пароль'
						]
					}
					break;
				case 'register':
					rules = {
						login: [
							value => !!value || 'Укажите Логин',
							value => /^[-._a-z0-9а-я]+@(?:[a-z0-9а-я][-a-z0-9а-я]+\.)+[a-zа-я]{2,6}$/.test(value.toLowerCase()) || 'Проверьте корректнность email',
						],
						password: [
							value => !!value || 'Введите пароль для учетной записи'
						],
						fio: [
							value => !!value || 'Укажите как к Вам обращаться',
						],
						/*mobile: [
							value => !!value || 'Укажите номер телефона для уточнения деталей заказа',
						],*/
						requisites: [
							value => {
								const isLegal = $form.find(`[name="legal"]`).prop('checked');
								return !isLegal || !!value || 'Укажите банковские реквизиты для выставления счета'
							}
						],
					}
					break;
				case 'update-info':
					rules = {
						login: [
							value => !!value || 'Укажите email',
							value => /^[-._a-z0-9а-я]+@(?:[a-z0-9а-я][-a-z0-9а-я]+\.)+[a-zа-я]{2,6}$/.test(value.toLowerCase()) || 'Проверьте корректнность email',
						],
						fio: [
							value => !!value || 'Укажите как к Вам обращаться',
						],
						/*mobile: [
							value => !!value || 'Укажите номер телефона для уточнения деталей заказа',
						],*/
						requisites: [
							value => {
								const isLegal = $form.find(`[name="legal"]`).prop('checked');
								return !isLegal || !!value || 'Укажите банковские реквизиты для выставления счета'
							}
						],
					}
					break;
				case 'update-pass':
					rules = {
						current_password: [
							value => !!value || 'Введите текущий пароль для учетной записи',
							value => {
								const isLegal = $form.find(`[name="legal"]`).prop('checked');
								return !isLegal || !!value || 'Укажите банковские реквизиты для выставления счета'
							}
						],
						new_password: [
							value => !!value || 'Введите новый пароль для учетной записи',
							value => {
								const isLegal = $form.find(`[name="legal"]`).prop('checked');
								return !isLegal || !!value || 'Укажите банковские реквизиты для выставления счета'
							}
						],
					}
					break;
				case 'restore':
					rules = {
						login: [
							value => !!value || 'Укажите Email',
							value => /^[-._a-z0-9а-я]+@(?:[a-z0-9а-я][-a-z0-9а-я]+\.)+[a-zа-я]{2,6}$/.test(value.toLowerCase()) || 'Проверьте корректнность email',
						]
					}
					break;
				case 'feedback':
					rules = {
						email: [
							value => !!value || 'Укажите Email, куда выслать новый пароль',
							value => /^[-._a-z0-9а-я]+@(?:[a-z0-9а-я][-a-z0-9а-я]+\.)+[a-zа-я]{2,6}$/.test(value.toLowerCase()) || 'Проверьте корректнность email',
						],
						fio: [
							value => !!value || 'Укажите как к Вам обращаться',
						],
						message: [
							value => !!value || 'Без сообщения письмо не имеет смысла :)',
						]
					}
					break;
				case 'pass-restore':
					rules = {
						new_password: [
							value => !!value || 'Задайте пароль',
						],
						repeat_password: [
							value => !!value || 'Повторите пароль',
							value => {
								const new_password = $form.find(`[name="new_password"]`).val();
								return new_password === value || 'Пароли не совпадают'
							}
						]
					}
					break;
				case 'confirm-order':
					rules = {
						mobile: [
							value => !!value || 'Номер телефона нужен для уточнения деталей заказа',
						],
						delivery: [
							value => {
								return !!value || 'Выберите форму доставки'
							},
						],
						address: [
							value => {
								const delivery = $form.find(`[name="delivery"]:checked`);
								const needDelivery = delivery.val() === '1';
								return !needDelivery || !!value || 'Укажите адрес для доставки'
							}
						],
					}
					break;
			}

			$form.validate(
				rules,
				async () => {

					if (!$form.hasClass('no-ajax')) {

						e.preventDefault();
						if ($submit.hasClass('is-loading')) return false;
						$submit.addClass('is-loading');

						let data = $form.serialize();

						if (
							formName === 'register'
							|| formName === 'feedback'
						) {
							try {
								const token = await window.grecaptcha.execute('6LfhxdYZAAAAAE9KdDF33pO-wkmI2Jh-WOS_L_DF', {action: 'homepage'});
								data = `${data}&g-recaptcha-response=${token}`;
							} catch (e) {
								console.error('grecaptcha is error', e)
							}
						}

						try {
							const res = await $.ajax({
								type: $form.attr('method'),
								url: $form.attr('action'),
								data
							});

							console.info('res', res)

							if ( res.errors && res.errors.length ){
								const text = res.errors.reduce((a, b) => `${a}<br>` + `${b}<br>`)
							} else {
								switch (formName) {
									case 'login':
										window.location.reload();
										break;
									case 'register':
										window.app.popup.open('.popup_info', res.message, false, true);
										break;
									case 'restore':
										window.app.popup.open('.popup_info', res.message, false, true);
										break;
									case 'feedback':
										window.app.popup.open('.popup_info', res.message, false, true);
										break;
									case 'pass-restore':
										window.app.popup.open('.popup_info', res.message, false, true);
										break;
									default:
										window.location.reload();
										break;
								}
							}
						} catch (e) {
							if (e.responseJSON && e.responseJSON.errors && e.responseJSON.errors.length){
								const text = e.responseJSON.errors.reduce((a, b) => `${a}<br>` + `${b}<br>`)
								console.info(text)
							}
						} finally {
							$submit.removeClass('is-loading')
						}
					}

				},
				(err) => {
					e.preventDefault();
					console.error(err)
				});

		})
	;



	$('[data-auto-load]').each(function () {
		const $form = $(this);
		const $container = $($form.attr('data-auto-load'));
		let done = false;
		let loading = false;

		$form.on('submit', (e)=>{
			e.preventDefault();
			submit();
		});

		endDetect();
		$(window).on('scroll resize', endDetect);

		function endDetect() {
			const $win = $(window);
			const elTop = $container.offset().top + $container.height();
			const vpTop = $win.scrollTop() + $win.height();

			if (vpTop > elTop && !loading){
				submit(true);
			}
		}

		function submit(append) {
			const data = $form.length ? $form.serializeArray() : [];

			if (append && done) return;
			if (loading) return;
			loading = true;
			$container.addClass('is-loading');

			data.push({
				name: 'limit',
				value: 12
			});
			data.push({
				name: 'offset',
				value: append ? $container.children().length : 0
			});

			if (!append) {
				done = false;
			}


			$.ajax({
				type: $form.attr('method'),
				url: $form.attr('action'),
				data: $.param(data),
				success: res =>{
					if (append) {
						$container.append(res.html);
					} else {
						window.scrollTo({
							top: 0,
							behavior: "smooth"
						});
						$container.html(res.html);
					}

					done = !!res.done;
				},
				error: (e)=>{

				},
				complete: ()=>{
					$container.removeClass('is-loading');
					setTimeout(()=>{
						loading = false;
					}, append ? 0 : 500)
				}
			});
		}
	});

	/**
	 * form methods
	 */

	$.fn.clear = function() {
		const $form = this.filter('form');
		$form.find('input[type="radio"], input[type="checkbox"]').prop('checked', false);
		$form.find('textarea').val('');
		$form.find('input[type="text"], input[type="email"]').val('');
	};

	$.fn.errors = function(err) {
		const $form = this.filter('form');
		const def = {
			wrapElClass: 'form-field',
			errClass: 'has-error',
			errElClass: 'form-error',
		};

		if (err === false) {
			$form.find(`.${def.wrapElClass}`)
				.removeClass(def.errClass)
				.find(`.${def.errElClass}`).remove()
			;
		} else {
			err.forEach(e => {
				const $input = $form.find(`[name="${e.name}"]`);

				if (e.errors && e.errors.length) {
					$input.closest(`.${def.wrapElClass}`)
						.addClass(def.errClass)
						.append(`<div class="${def.errElClass}">${e.errors[0]}</div>`)
					;
				} else {
					$input.closest(`.${def.wrapElClass}`)
						.removeClass(def.errClass)
						.find(`.${def.errElClass}`).remove()
					;
				}

			});
		}
	};

	$.fn.validate = async function(param, resolve = ()=>{}, reject = ()=>{}) {

		try {
			const $form = this.filter('form');
			const errors = {};
			const result = {};
			const data = $form.serializeArray();

			console.info($form.find('input[type=radio]'), data);

			$form.find('input[type=radio], input[type=checkbox]').each(function() {
				const $input = $(this);
				const name = $input.attr('name');
				const exist = data.filter(i => i.name = name);
				if (!exist.length) {
					data.push({
						name,
						value: ''
					})
				}
			})

			$form.errors(false);

			data.forEach(item => {
				if (result[item.name]){
					if (!result[item.name].push) {
						result[item.name] = [result[item.name]];
					}
					result[item.name].push(item.value || '');
				} else {
					result[item.name] = item.value || '';
				}
			});

			Object.keys(param).forEach(name => {
				const $field = $form.find(`[name="${name}"]`);
				if (param[name] && $field.length) {
					param[name].forEach(func => {
						const valid = func(result[name]);
						const isExist = result[name] !== undefined;
						const isVisible = $field.is(':visible');
						const isNoValid = valid !== true;

						if (isExist && isNoValid && isVisible) {
							if (!errors[name]) {
								errors[name] = [];
							}
							errors[name].push(valid);
						}
					})
				}
			});

			const parsedErrors = Object.keys(errors).map(name => {
				return {
					name,
					errors: errors[name]
				}
			});

			if (parsedErrors.length) {
				$form.errors(parsedErrors);
				reject(parsedErrors);
			} else {
				resolve();
			}

		} catch (err) {
			console.info(err)
			reject()
		}
	};

});