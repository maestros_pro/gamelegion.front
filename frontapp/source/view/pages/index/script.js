import './style.scss'

import '../../../assets/img/offer/cert-0.png';
import '../../../assets/img/offer/cert-250.png';
import '../../../assets/img/offer/cert-1000.png';
import '../../../assets/img/offer/cert-3000.png';
import '../../../assets/img/offer/cert-5000.png';

import Picker from '../../modules/picker'

$(()=>{

	class Offer {

		constructor() {
			this.$el = {};
			const that = this;

			this.data = {};

			this.data = new Proxy({
				offerStep: undefined,
				offerPrice: '',
				offerMessage: '',
				offerTarget: '',
				offerFriendName: '',
				offerFriendPhone: '',
				offerFriendEmail: '',
				offerSelfPhone: '',
				offerSelfEmail: '',
				offerBillEmail: '',
				offerConfirm: '',
				offerTime: '',
				offerDateValue: '',
			}, {
				set(target, property, value) {
					target[property] = value;
					const $input = that.$el.dialog.find(`[name=${property}]`);
					switch (property) {
						case 'offerStep':
							if (parseInt(value) >= 6) {
								that.$el.dialog.find('.offer-dialog__aside').addClass('is-active');
							} else {
								that.$el.dialog.find('.offer-dialog__aside').removeClass('is-active');
							}
							break;
						case 'offerPrice':
							const $priceInput = $input.filter(':checked');
							if ($priceInput.length){
								const image = $priceInput.attr('data-card-image');
								that.$el.dialog.find('.js-offer-output-price').html(`${value} руб.`);
								if (image) that.$el.dialog.find('.js-offer-output-image').attr('src', image);
							}
							break;
						case 'offerMessage':
							value = value.slice(0, 360);
							that.$el.dialog.find('.offer-message__counter').html(`${value.length}/360`);
							that.$el.dialog.find('.js-offer-output-message').html(`${value}`);
							$input.val(value);
							break;
						case 'offerTarget':
							if (value === 'self') {
								that.$el.dialog.find('.js-offer-target-name').html(`ВАШИ КОНТАКТНЫЕ ДАННЫЕ`);
							} else if (value === 'friend') {
								that.$el.dialog.find('.js-offer-target-name').html(`КОНТАКТНЫЕ ДАННЫЕ ДРУГА`);
							}
							that.$el.dialog.find(`[data-offer-contact]`).hide();
							that.$el.dialog.find(`[data-offer-contact=${value}]`).show();
							break;
						case 'offerFriendName':
						case 'offerFriendPhone':
						case 'offerFriendEmail':
						case 'offerSelfPhone':
						case 'offerSelfEmail':
							let string = [];
							if (target.offerTarget === 'friend') {
								if (target.offerFriendName) {
									string.push(target.offerFriendName)
								}
								if (target.offerFriendPhone) {
									string.push(target.offerFriendPhone)
								}
								if (target.offerFriendEmail) {
									string.push(target.offerFriendEmail)
								}
							} else {
								if (target.offerSelfPhone) {
									string.push(target.offerSelfPhone)
								}
								if (target.offerSelfEmail) {
									string.push(target.offerSelfEmail)
								}
							}
							that.$el.dialog.find('.js-offer-output-author').html(`${string.join(', ')}.`);
							break;
						case 'offerTime':

							if (value === 'now') {
								that.$el.dialog.find('.js-offer-output-date').html('Сейчас');
							}
							break;
					}

					if (parseInt(target.offerStep) >= 6 && !!target.offerTime) {
						that.$el.dialog.find('.offer-output__foot').removeClass('is-disabled');
					} else {
						that.$el.dialog.find('.offer-output__foot').addClass('is-disabled');
					}

					return true;
				}
			});
		}

		initDOM() {
			this.$el.dialog = $('.offer-dialog');
			this.$el.step = {};
			this.$el.step.all = this.$el.dialog.find('.offer-step__item');
			this.$el.step.all.each((i, e) => {
				this.$el.step[e.getAttribute('data-step')] = $(e);
			});
		}

		initEVENT() {
			this.$el.dialog
				.on('click', '[data-go-step]', (e) => {
					const $t = $(e.target);
					const step = $t.attr('data-go-step');
					const $form = $t.closest('form');


					if (step < this.data.offerStep) {
						this.open(step);
						return;
					}

					$form.validate(
						{
							offerPrice: [
								value => !!value || 'Выберите сертификат',
							],
							offerMessage: [
								value => !!value || 'Добавьте комментарий',
							],
							offerTarget: [
								value => !!value || 'Выберите кому будете дарить сертификат',
							],
							/*offerFriendPhone: [
								value => !!value || 'Укажите номер телефона',
							],
							offerFriendEmail: [
								value => !!value || 'Укажите Email',
								value => /^[-._a-z0-9а-я]+@(?:[a-z0-9а-я][-a-z0-9а-я]+\.)+[a-zа-я]{2,6}$/.test(value.toLowerCase()) || 'Проверьте корректнность email',
							],*/
							offerFriendName: [
								value => !!value || 'Укажите имя',
							],
							/*offerSelfPhone: [
								value => !!value || 'Укажите номер телефона',
							],*/
							offerSelfEmail: [
								value => !!value || 'Укажите Email',
								value => /^[-._a-z0-9а-я]+@(?:[a-z0-9а-я][-a-z0-9а-я]+\.)+[a-zа-я]{2,6}$/.test(value.toLowerCase()) || 'Проверьте корректнность email',
							],
							/*offerBillEmail: [
								value => !!value || 'Укажите Email',
								value => /^[-._a-z0-9а-я]+@(?:[a-z0-9а-я][-a-z0-9а-я]+\.)+[a-zа-я]{2,6}$/.test(value.toLowerCase()) || 'Проверьте корректнность email',
							],*/
							offerTime: [
								value => !!value || 'Выберите время',
							],
						},
						async () => {
							if (step) {
								this.open(step);
							}
						},
						(err) => {
							e.preventDefault();
							console.error(err)
						}
					);
				})

				.on('click', '.offer-dialog__close', (e) => {
					$('body').removeClass('offer-show');
				})

				.on('click', '[data-offer-submit]', (e) => {
					const $t = $(e.target);
					const disabled = $t.closest('.is-disabled').length;
					const $form = $t.closest('form');
					if (disabled) return false;
					$form.errors(false);
					$form.validate(
						{
							offerConfirm: [
								value => !!value || 'Необходимо согласиться с условиями',
							],
						},
						async () => {
							alert('Оппачки')
						},
						(err) => {
							e.preventDefault();
							console.error(err)
						}
					);
				})

				.on('change input', 'input, textarea', (e) => {
					const $input = $(e.target);
					const $form = $input.closest('form');
					$form.errors(false);
					const name = $input.attr('name');
					if (name && this.data[name] !== undefined) {
						this.data[name] = $input.val();
					}
				})

				.on('click', '.js-picker-button', (e) => {
					if (this.picker) this.picker.open();
				})

			;
		}

		init () {
			this.initDOM();
			this.initEVENT();

			this.picker = new Picker({
				select: (data) => {
					$('[name=offerTime]').filter('[value=time]').prop('checked', true).trigger('change');
					this.data.offerDateValue = data.text;
					this.$el.dialog.find('.js-offer-output-datebtn').html(`${this.data.offerDateValue}`);
					this.$el.dialog.find('.js-offer-output-date').html(`${this.data.offerDateValue}`);
				},
				cancel: () => {
					$('[name=offerTime]').filter('[value=now]').prop('checked', true).trigger('change');
				}
			});
		}

		open(step = 1) {
			this.data.offerStep = step;
			$('body').addClass('offer-show');
			this.$el.step.all.removeClass('is-active');
			this.$el.step[step].addClass('is-active');
		}
	}

	const offer = new Offer();

	offer.init();

	$('body')
		.on('click', '[data-side-open]', e => {
			const $t = $(e.target);
			const name = $t.attr('data-side-open');

			$(`[data-side-item=${name}]`).addClass('is-open');
		})
		.on('click', '[data-side-close]', e => {
			const $t = $(e.target);
			$t.closest('[data-side-item]').removeClass('is-open');
		})
		.on('click', '[data-offer-open]', function(e) {
			e.preventDefault();
			const $t = $(this);
			const value = $t.attr('data-offer-open');
			$(`[name=offerPrice]`).filter(function() {return $(this).val() === value}).prop('checked', true).trigger('change');
			offer.open();
		})
	;

	$(document).ready(function(){
		$('.modal').modal({
			opacity: 1
		});
	});

});