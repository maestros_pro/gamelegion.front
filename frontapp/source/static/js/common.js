$('.prod_all').on(`init reInit`, function (event, slick) {
    $('.counterGun').text(1 + ' / ' + slick.slideCount);
});
$('.prod_all').on(`afterChange`, function (event, slick, currentSlide, nextSlide) {
    $('.counterGun').text(currentSlide + 1 + ' / ' + slick.slideCount);
});
$('.prod_all').slick({
    dots: false,
    infinity: false,
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.popgun',
    prevArrow: $('.prod_left'),
    nextArrow: $('.prod_right'),
    responsive: [{
            breakpoint: 1500,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 768,
            settings: {
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                arrows: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                centerMode: true,
                centerPadding: '80px'
            }
        }
    ]
});


$('.popgun').on(`init reInit`, function (event, slick) {
    $('.counterGunner').text(1 + ' / ' + slick.slideCount);
});
$('.popgun').on(`afterChange`, function (event, slick, currentSlide, nextSlide) {
    $('.counterGunner').text(currentSlide + 1 + ' / ' + slick.slideCount);
});
$('.popgun').slick({
    dots: true,
    infinity: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.prod_all',
    prevArrow: $('.popgun_arrow'),
    nextArrow: $('.popgun_arrow'),
    responsive: [{
            breakpoint: 768,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                arrows: true,
                dots: false,
                slidesToShow: 1
            }
        }
    ]
});

$(".popup_gun_1").modaal({
    hide_close: true,
    custom_class: 'gun_popups',
    before_open: function () {}
});


$(document).ready(function () {
    $('.prod_all .slick-slider').slick();

    $('.prod_all .slick-slide').on('click', function () {
        $('.popgun').slick('slickGoTo', $(this).data('slickIndex'));
    });
});

$('.gallery_slider').on(`init reInit`, function (event, slick) {
    $('.counterGall').text(1 + ' / ' + slick.slideCount);
});
$('.gallery_slider').on(`afterChange`, function (event, slick, currentSlide, nextSlide) {
    $('.counterGall').text(currentSlide + 1 + ' / ' + slick.slideCount);
});
$('.gallery_slider').slick({
    dots: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    infinity: false,
    prevArrow: $('.gall_left'),
    nextArrow: $('.gall_right'),
    responsive: [{
            breakpoint: 768,
            settings: {
                arrows: false,
                dots: false,
                slidesToShow: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                dots: false,
                slidesToScroll: 1
            }
        }
    ]
});

$('.guns_slider').on(`init reInit`, function (event, slick) {
    $('.counterGunsGall').text(1 + ' / ' + slick.slideCount);
});
$('.guns_slider').on(`afterChange`, function (event, slick, currentSlide, nextSlide) {
    $('.counterGunsGall').text(currentSlide + 1 + ' / ' + slick.slideCount);
});
$('.guns_slider').slick({
    dots: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    infinity: false,
    prevArrow: $('.gallGuns_left'),
    nextArrow: $('.gallGuns_right'),
    responsive: [{
            breakpoint: 768,
            settings: {
                arrows: false,
                dots: false,
                slidesToShow: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                dots: false,
                slidesToScroll: 1
            }
        }
    ]
});

$('.cert_all').on(`init reInit`, function (event, slick) {
    $('.counterCert').text(1 + ' / ' + slick.slideCount);
});
$('.cert_all').on(`afterChange`, function (event, slick, currentSlide, nextSlide) {
    $('.counterCert').text(currentSlide + 1 + ' / ' + slick.slideCount);
});
$('.cert_all').slick({
    dots: false,
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: $('.cert_left'),
    nextArrow: $('.cert_right'),
    centerMode: false,
    centerPadding: '0px',
    responsive: [{
            breakpoint: 1500,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 768,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 3
            }
        },
        {
            breakpoint: 480,
            settings: {
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                centerMode: true,
                centerPadding: '80px'
            }
        }
    ]
});


$('.gall').lightcase();


$('.menu_fixed').click(function () {
    $('body').addClass('is_menu_open');
});

$('.privacy_link').click(function (e) {
    e.preventDefault();
    $('body').addClass('is_privacy_open');
});

$('.menu_close').click(function () {
    $('body').removeClass('is_menu_open');
});

$('.menu_close_privacy').click(function () {
    $('body').removeClass('is_privacy_open');
});



$('.thanksLinkCert').click(function (e) {
    e.preventDefault();

    var uname = $('input[name="phone"]').val();
    if (!uname == '') {
        $(".thanksLinkCert").modaal({
            content_source: '#popup_thanks',
            hide_close: true,
            before_open: function () {

                let msg = $(this).serialize();
                $.ajax({
                    type: 'POST',
                    url: 'form_cert.php',
                    data: msg,
                    beforeSend: function () {

                    },

                    success: function (data) {
                        $('.popup_inform').modaal('close');
                    },

                    complete: function () {
                        $('.loading').hide();
                    },
                    error: function (xhr, str) {
                        alert('Возникла ошибка: ' + xhr.responseCode);
                    }
                });
            },
        });

        return false;
    } else {
        alert('не заполнены поля');
        return true;
    }
});


$(".popup_inform").modaal({
    hide_close: true,
    before_open: function () {
        $('body').removeClass('is_menu_open');
        $('.popup_gun_1').modaal('close');
    },
});

$(".popup_cert_1").modaal({
    hide_close: true
});
$(".popup_cert_2").modaal({
    hide_close: true
});
$(".popup_cert_3").modaal({
    hide_close: true
});
$(".popup_cert_4").modaal({
    hide_close: true
});

$('.tab').click(function () {
    $('.gallery_slider').slick('refresh');
    $('.guns_slider').slick('refresh');
});

$(function () {
    $('.nav_item a').on('click', function (event) {
        // отменяем стандартное действие
        event.preventDefault();

        $('body').removeClass('is_menu_open');

        var sc = $(this).attr("href"),
            dn = $(sc).offset().top;
        /*
         * sc - в переменную заносим информацию о том, к какому блоку надо перейти
         * dn - определяем положение блока на странице
         */

        $('html, body').animate({
            scrollTop: dn
        }, 1000);

        /*
         * 1000 скорость перехода в миллисекундах
         */
    });
});

$(function () {
    $('.ft_item a').on('click', function (event) {
        // отменяем стандартное действие
        event.preventDefault();

        $('body').removeClass('is_menu_open');

        var sc = $(this).attr("href"),
            dn = $(sc).offset().top;
        /*
         * sc - в переменную заносим информацию о том, к какому блоку надо перейти
         * dn - определяем положение блока на странице
         */

        $('html, body').animate({
            scrollTop: dn
        }, 1000);

        /*
         * 1000 скорость перехода в миллисекундах
         */
    });
});

$(document).ready(function () {

    [].forEach.call(document.querySelectorAll('input[name="phone"]'), function (input) {
        var keyCode;

        function mask(event) {
            event.keyCode && (keyCode = event.keyCode);
            var pos = this.selectionStart;
            if (pos < 3) event.preventDefault();
            var matrix = "+7 (___) ___ ____",
                i = 0,
                def = matrix.replace(/\D/g, ""),
                val = this.value.replace(/\D/g, ""),
                new_value = matrix.replace(/[_\d]/g, function (a) {
                    return i < val.length ? val.charAt(i++) || def.charAt(i) : a
                });
            i = new_value.indexOf("_");
            if (i != -1) {
                i < 5 && (i = 3);
                new_value = new_value.slice(0, i)
            }
            var reg = matrix.substr(0, this.value.length).replace(/_+/g,
                function (a) {
                    return "\\d{1," + a.length + "}"
                }).replace(/[+()]/g, "\\$&");
            reg = new RegExp("^" + reg + "$");
            if (!reg.test(this.value) || this.value.length < 5 || keyCode > 47 && keyCode < 58) this.value = new_value;
            if (event.type == "blur" && this.value.length < 5) this.value = ""
        }

        input.addEventListener("input", mask, false);
        input.addEventListener("focus", mask, false);
        input.addEventListener("blur", mask, false);
        input.addEventListener("keydown", mask, false)

    });
});

// Tabs
$(".tab_item").not(":first").hide();
$(".wrapper .tab").click(function () {
    $(".wrapper .tab").removeClass("active").eq($(this).index()).addClass("active");
    $(".tab_item").hide().eq($(this).index()).fadeIn()
}).eq(0).addClass("active");


$(function () {
    if ($(window).width() < 500) {
        ymaps.ready(function () {
            var myMap = new ymaps.Map('map', {
                    center: [59.924397, 30.242036],
                    zoom: 17,
                    controls: []

                }, {
                    searchControlProvider: 'yandex#search'
                }),

                // Создаём макет содержимого.
                MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
                    '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
                ),

                myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
                    hintContent: 'Собственный значок метки',
                    balloonContent: 'Васильевский остров Кожевенная линия 40, территория Севкабель Порт'
                }, {
                    // Опции.
                    // Необходимо указать данный тип макета.
                    iconLayout: 'default#image',
                    // Своё изображение иконки метки.
                    iconImageHref: 'img/balun.png',
                    // Размеры метки.
                    iconImageSize: [82, 122],
                    // Смещение левого верхнего угла иконки относительно
                    // её "ножки" (точки привязки).
                    iconImageOffset: [-20, -50]
                });

            myMap.geoObjects.add(myPlacemark);

            myMap.panTo(
                // Координаты нового центра карты
                [59.924900, 30.241100], {
                    /* Опции перемещения:
                       разрешить уменьшать и затем увеличивать зум
                       карты при перемещении между точками 
                    */
                    flying: true
                }
            )

            myMap.behaviors.disable(['scrollZoom', 'drag', 'rightMouseButtonMagnifier']);

            myMap.controls.add('zoomControl');
        });
    } else {
        ymaps.ready(function () {
            var myMap = new ymaps.Map('map', {
                    center: [59.924397, 30.242036],
                    zoom: 17,
                    controls: []

                }, {
                    searchControlProvider: 'yandex#search'
                }),

                // Создаём макет содержимого.
                MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
                    '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
                ),

                myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
                    hintContent: 'Собственный значок метки',
                    balloonContent: 'Васильевский остров Кожевенная линия 40, территория Севкабель Порт'
                }, {
                    // Опции.
                    // Необходимо указать данный тип макета.
                    iconLayout: 'default#image',
                    // Своё изображение иконки метки.
                    iconImageHref: 'img/balun.png',
                    // Размеры метки.
                    iconImageSize: [82, 122],
                    // Смещение левого верхнего угла иконки относительно
                    // её "ножки" (точки привязки).
                    iconImageOffset: [-20, -50]
                });

            myMap.geoObjects.add(myPlacemark);

            myMap.panTo(
                // Координаты нового центра карты
                [59.924500, 30.240000], {
                    /* Опции перемещения:
                       разрешить уменьшать и затем увеличивать зум
                       карты при перемещении между точками 
                    */
                    flying: true
                }
            )

            myMap.behaviors.disable(['scrollZoom', 'drag', 'rightMouseButtonMagnifier']);

            myMap.controls.add('zoomControl');
        });
    }
});