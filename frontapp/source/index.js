import '~/assets/scss/style.scss'

/**
 * modules
 */

import '~/view/modules/form/script.js'

/**
 * main layout
 */

/**
 * sections
 */

import '~/view/pages/index/script.js'

// import fakeServer from './fake-api';
// if (IS_LOCAL_ENV) {
// 	fakeServer()
// }
